import pandas as pd
from collections import Counter
from utils import Parser

class CodeChallengeImplementation:
    """
    Insert the logic for the coding challenge, so that main function in main.py runs without errors.
    """
    def __init__(self):
        """ Initiates the necessary variables """
        self.states = pd.DataFrame(columns=['channel', 'switch',
                            'average', 'majority'])
        self.parser = Parser()

    def merge(self, content: str) -> None:
        """ Add logic that handles a new incoming message. """
        data = self.parser.parse(content)
        self.states = self.states.append(pd.Series(data), ignore_index=True)

    def state(self) -> dict:
        """ Return the current merged state. """
        return self._get_state(self.states)

    def reversed_state(self) -> dict:
        """ Return the state if the messages were received in the reversed order. """
        return self._get_state(self.states[::-1])

    def _get_state(self, df: pd.DataFrame) -> dict:
        """ Handle the determination of the state given an ordered DataFrame """
        current_channel_state = df.groupby('channel').last()
        combined_state = {
            "switch": self._get_switch(df),
            "average": self._get_average(current_channel_state),
            "majority": self._get_majority(current_channel_state)
        }
        return combined_state

    def _get_switch(self, df: pd.DataFrame) -> bool:
        """ Return the value of 'switch' from the last received message """
        return df.iloc[-1]['switch']

    def _get_average(self, current_channel_state: pd.DataFrame) -> float:
        """ Return the average of the current values over all channels """
        return current_channel_state['average'].mean()

    def _get_majority(self, current_channel_state: pd.DataFrame) -> str:
        """ Return the category that is currently shared by the most channels """
        counter = Counter(current_channel_state['majority'])
        return counter.most_common(1)[0][0]