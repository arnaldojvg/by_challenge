# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 12:40:14 2020

@author: arnal
"""
import sys
import pandas as pd
from collections import Counter
sys.path.insert(0, "..")
import solutions
from code_challenge import CodeChallengeImplementation

# this is the state of 1,2,3,4 test case
test_state = pd.read_csv('test_df.csv',indeex_col=0)

read_cases = [
    '{"channel": 2, "switch": false, "average": 72, "majority": "bar"}',
    '{"channel": 2, "switch": True, "average": 85, "majority": "baz"}',
    '<?xml version="1.0" encoding="UTF-8" ?><root><channel type="int">2</channel><switch type="bool">True</switch><average type="int">62</average><majority type="str">bar</majority></root>',
]

def test_merge():

    merger = CodeChallengeImplementation()
    for st in read_cases:
        merger.merge(st)
    
    assert len(merger) == len(test_state)

def test_get_switch():

    merger = CodeChallengeImplementation()
    merger.states = test_state.copy()

    assert merger._get_switch() == test_state.iloc[-1]['switch']

def test_get_average():
    
    merger = CodeChallengeImplementation()
    merger.states = test_state.copy()

    # Assert 1 of calculation of avg
    assert merger._get_average() == test_state['average'].mean()

def test_get_majority():
    
    merger = CodeChallengeImplementation()
    merger.states = test_state.copy()

    # Assert 1 of calculation of avg
    assert merger._get_amajority() == Counter(test_state['majority']).most_common(1)[0][0]

