# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 12:20:47 2020

@author: arnal
"""
import sys
sys.path.insert(0, "..")
from utils import Parser, TYPES

# Here we have the three different inputs detected
read_cases = [
    '{"channel": 2, "switch": false, "average": 72, "majority": "bar"}',
    '{"channel": 2, "switch": True, "average": 85, "majority": "baz"}',
    '<?xml version="1.0" encoding="UTF-8" ?><root><channel type="int">2</channel><switch type="bool">True</switch><average type="int">62</average><majority type="str">bar</majority></root>',
]

# Constant cases
json_str = read_cases[0]
literal_str = read_cases[1]
xml_str = read_cases[2]


def assert_types(d):
    
    assert isinstance(d["channel"], TYPES["channel"])
    assert isinstance(d["switch"], TYPES["switch"])
    assert isinstance(d["average"], TYPES["average"])
    assert isinstance(d["majority"], TYPES["majority"])


def test_get_child_value():

    parser = Parser()
    
    d = {
        "channel": parser._get_child_value("channel", 1000e2)
        "switch": parser._get_child_value("switch", -1),
        "average": parser._get_child_value("average", 55.5),
        "majority": parser._get_child_value("majority", True)
    }

    assert_types(d)

def test_validate_data():
    
    parser = Parser()
    d = {
        "channel": True,
        "switch": "False",
        "average": 55.45,
        "majority": 1000
    }

    validated_data = parser._validate_data(d)
    assert_types(validated_data)

def test_parse_literal():

    parser = Parser()

    parsed_literal = parser._parse_literal(literal_str)
    assert_types(parsed_literal)

def test_parse_json():

    parser = Parser()

    parsed_json = parser._parse_json(json_str)
    assert_types(parsed_json)

def test_parse_xml():

    parser = Parser()

    parsed_xml = parser._parse_xml(xml_str)
    assert_types(parsed_xml)

def test_parse():

    parser = Parser()

    for st in read_cases:
        assert_types(parser.parse(st))