# -*- coding: utf-8 -*-
"""
Created on Thu Sep 25 12:17:11 2020

@author: arnal
"""
import json
from ast import literal_eval
import xml.etree.ElementTree as ET

TYPES = {
        "channel": str,
        "switch": bool,
        "average": int,
        "majority": str
    }

class Parser:
    """
    Parses the content from a source to a useful dict
    """    
    def parse(self, content: str) -> dict:
        """ Return parsed content trying different formats """
        try:
            return self._parse_literal(content)
        except (ValueError, SyntaxError):
            try:
                return self._parse_json(content)
            except json.JSONDecodeError:
                return self._parse_xml(content)
    
    def _parse_literal(self, content: str) -> dict:
        """ Parses the format from a dict literal """
        data = literal_eval(content)
        return self._validate_data(data)
        
    def _parse_json(self, content: str) -> dict:
        """ Parses the format in json """
        data = json.loads(content)
        return self._validate_data(data) 
       
    def _parse_xml(self, content: str) -> dict:
        """ Parses the date in xml """
        etree = ET.fromstring(content)
        
        data = {child.tag: self._get_child_value(child.tag, child.text)
                for child in etree}
        return data
    
    def _validate_data(self, data: dict) -> dict:
        """ Forces every field type for consistency while grouping data """
        return {key: self._get_child_value(key, value)
                for key, value in data.items()}
    
    @staticmethod
    def _get_child_value(key: str, value: str):
        """ Returns the value of the child in proper type """
        # We use the constant TYPES to proper convert types
        # To explain what this does, every key returns a type
        # eg. int, and if we do int(value) it changes it to integer.

        converter = TYPES[key]
        # Validation in case the int value comes as a float string
        if isinstance(converter, int) and isinstance(value, str):
            value = float(value)
            return converter(value)
        
        # Validation for string representation of booleans
        if isinstance(converter, bool):
            if value == 'True' or value == 'true':
                return True
            if value == 'False' or value == 'false':
                return False

        return converter(value)
